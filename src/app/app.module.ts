import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { BasicTreeComponent } from './basic-tree/basic-tree.component';
import { TreeModule } from 'angular-tree-component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    TreeModule.forRoot()
  ],
  declarations: [
    AppComponent,
    BasicTreeComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
