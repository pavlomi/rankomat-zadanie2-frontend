import {NodeDto} from './nodeDto';

export class Node{
  id: number;
  name: string;
  children?: Node[];

  constructor(nodeDto: NodeDto) {
    this.id = nodeDto.id;
    this.name = nodeDto.name;
    this.children = nodeDto.nodes.map(n => new Node(n))
  }
}
