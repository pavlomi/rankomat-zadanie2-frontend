import { Component, OnInit } from '@angular/core';
import {NodeService} from '../node.service';
import {Node} from '../node';

@Component({
  selector: 'app-basic-tree',
  template: '<tree-root [nodes]="nodes" [options]="options"></tree-root>',
  styleUrls: ['./basic-tree.component.css']
})
export class BasicTreeComponent implements OnInit{

  nodes: Node[] =  [];
  constructor(private nodeService: NodeService) { }

  ngOnInit() {
    this.getNodes()
  }

  getNodes(): void  {
    this.nodeService.getNodes().subscribe(nodesDto => this.nodes = nodesDto.map(n => new Node(n)))
  }

    // [
    // {
    //   id: 1,
    //   name: 'root1',
    //   children: [
    //     { id: 2, name: 'child1' },
    //     { id: 3, name: 'child2' }
    //   ]
    // },
    // {
    //   id: 4,
    //   name: 'root2',
    //   children: [
    //     { id: 5, name: 'child2.1' },
    //     {
    //       id: 6,
    //       name: 'child2.2',
    //       children: [
    //         { id: 7, name: 'subsub' }
    //       ]
    //     }
    //   ]
    // }
  // ];

  options = {};
}
