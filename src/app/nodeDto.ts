export class NodeDto {
  id: number;
  name: string;
  nodes: NodeDto[];
}
